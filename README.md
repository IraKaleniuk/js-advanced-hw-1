## 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

Якщо зробити об'єкт А прототипом Б, то Б успадкує всі властивості та методи об'єкт А. Це дозволяє не дублювати код при 
створенні багатьох однакових об'єктів, а також дозволяє заощадити пам'ять, бо не доводить в кожному з тих об'єктів 
зберігати властивості та методи, які є в прототипі.
## 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?

Конструктор дочірнього класу не створює порожній об'єкт і не присвоює йому `this`,
оскільки очікує, що це зробить батьківський клас. Для того, щоб запустити батьківський
конструктор використовують `super()`.