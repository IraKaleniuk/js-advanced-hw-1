"use strict";

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age;
    }
    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary;
    }
    set salary(value) {
        this._salary = value;
    }
}

class Programmer  extends Employee {
    constructor(name, age, salary, languages) {
        super(name, age, salary);
        this._languages = languages;
    }

    set salary(value) {
        this._salary = value;
    }

    get salary() {
        return this._salary * 3;
    }

    get languages () {
        return this._languages;
    }
    set languages (value) {
        return this._languages = value;
    }

}

const prog1 = new Programmer("Ira", 22, 1000, ["js", "c++"]);
console.log(prog1);
console.log(prog1.salary);

const prog2 = new Programmer("Andrew", 25, 5000, ["js", "c++", "python", "ts"]);
console.log(prog2);
console.log(prog2.salary);

const prog3 = new Programmer("Oleg", 35, 9000, ["js", "c++", "python", "ts", "c#", "c"]);
console.log(prog3);
console.log(prog3.salary);